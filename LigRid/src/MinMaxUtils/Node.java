package MinMaxUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import field.Field;
import move.MoveType;

public class Node {

	private Node parent;
	private List<Node> children;
	private Field field;
	private MoveType move;
	private String type;
	private Integer score;
	private MoveType preMove;

	public Node() {
	}

	public Node(Node parent, Field field, MoveType move, String type) {
		this.parent = parent;
		this.field = field;
		this.move = move;
		this.type = type;
		this.score = null;
		this.children = new ArrayList<>();
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public void setChild(Node child) {
		this.children.add(child);
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public MoveType getMove() {
		return move;
	}

	public void setMove(MoveType move) {
		this.move = move;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public MoveType getPreMove() {
		return preMove;
	}

	public void setPreMove(MoveType preMove) {
		this.preMove = preMove;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Integer calculateScore() {
        if(children.isEmpty() && score ==null) return null; //if node has not got children nor score, return null.
        if(this.score != null) return score;// if node has score, return score.
        
        //Else calculate node's score from its children.
        List<Node> useless=new ArrayList<>();
        if(!children.isEmpty())
	        for(Node n: children) {
	        	Integer temp= n.calculateScore();
	        	if(temp == null)  useless.add(n);
	        }
        
        children.removeAll(useless);
        if(children.isEmpty()) return null;
        
		int finalScore = children.get(0).score;
		
		for(Node n : children) {
			Integer temp = n.score;
			if(temp == null) continue;
			if ((this.type.equals("MAX") && temp > finalScore) || (this.type.equals("MIN") && temp < finalScore)) {
				finalScore = temp;
			}
		}

		this.score = finalScore;
		return finalScore;
	}

}
