package MinMaxUtils;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import bot.BotState;
import field.Field;
import move.MoveType;

public class Algorithm {
	static int height ;
	static int width ;
	static long startTime;
	/**
	 * This is the manager method. First instantiate a root node. Then call
	 * MinMaxSetChildren to construct the hole tree.(Here, leafs' score has been
	 * only calculate) For each root's children calculate score by node-type (Min
	 * Max). By defult, root type is MAX so we search the bigger score of each move.
	 * 
	 * @param field
	 *            -The current state of game.
	 * @param state
	 *            - state of bot
	 * @param nextMoves
	 *            -How much deep the algorithm has to search. (How many MAX-levels
	 *            will be construct.)
	 * @return MoveType -The best available move for current state
	 */
	public static MoveType minmaxmanager(Field field, int nextMoves, BotState state) {
		startTime = System.currentTimeMillis();
		// set the parent
		height = field.getHeight();
		width = field.getWidth();
		Node parent = new Node(null, field, null, "MAX");
		// set children
		MinMaxSetChildren(parent, nextMoves);
		// calculate score for children and find the best.
		int score = Integer.MIN_VALUE;
		MoveType mv = null;
		for (Node c : parent.getChildren()) {
			if(System.currentTimeMillis() - startTime> 700){
			    System.err.println("Fast Forword."+state.getRoundNumber());
			    return MoveType.PASS;
			}
			Integer temp = c.calculateScore();
			if (temp != null)
				if (temp > score) {
					score = temp;
					mv = c.getMove();
				}
		}
		return mv;
	}

	/**
	 * This method implements a recursion. For each available move of a node,
	 * construct a node. If new node is a leaf, calculate its score.
	 * 
	 * @param state
	 *            - state of bot
	 * @param parent
	 *            -Current state.
	 * @param nextMoves
	 *            -How much deep the algorithm has to search. (How many MAX-levels
	 *            will be construct.)
	 */
	private static void MinMaxSetChildren(Node parent, int nextMoves) {

		if (parent.getType().equals("MAX"))
			nextMoves--;
		// find available
		ArrayList<MoveType> forbiddens = findForbiddens(parent.getField());
		ArrayList<MoveType> available_moves = new ArrayList<>(
				Arrays.asList(MoveType.DOWN, MoveType.UP, MoveType.LEFT, MoveType.RIGHT));
		available_moves.removeAll(forbiddens);
		// for each available
		if (!available_moves.isEmpty()) {
			for (MoveType mv : available_moves) {
				Node child = makeNextPossibleMove(mv, parent);
				
				if (parent.getParent()!=null)//only for findScroceApplyTactics search
					child.setPreMove(parent.getParent().getMove());
				
				parent.setChild(child);
				if (nextMoves != 0)
					MinMaxSetChildren(child, nextMoves);
				else if (nextMoves == 0) {
//					int score = findScore(child, mv, 2);
//					int score = findScoreByFrontEmptyBlocks(child, mv, 4);
//					int score = findScroceApplyTactics(child, mv, 3);
					int score= findScroceZigZagGreedy(child,mv,4);
					child.setScore(score);
				}
			}
		}else{
			if(parent.getType().equals("MAX")){
				parent.setScore(Integer.MIN_VALUE + 1);
			}else{
				parent.setScore(Integer.MAX_VALUE);
			}
		}
	}
	
	/**
	 * Another heuristic algorithm. Better move is decided upon one of the following
	 * tactics(observed after some matches). 1. Zig - zag , save time => useful at
	 * the beginning or when you have 2-3 tiles available on the grip(especially
	 * when there is little space left). 2. There are danger spots on the corners
	 * that should be left free by the end of the game(not get filled altogether by
	 * the end of the game). 3. Try to steel space of your opponent's grid space -
	 * you might have the change to trap him
	 * 
	 * @param leaf
	 * @param move
	 * @param loops
	 *            -How long algorithm has to search.
	 * @return
	 */
	public static int findScroceApplyTactics(Node leaf, MoveType move, int loops) {
		if (loops == 0)
			return 0;
		MoveType previousMove = leaf.getPreMove();
		int score = 0;
		int height = leaf.getField().getHeight();
		int width = leaf.getField().getWidth();
		String[][] grid = leaf.getField().getField();
		int column = (int) leaf.getField().getOpPosition().getX();// Leaf is a min node. So we have to find the score of
																	// opponent.
		int line = (int) leaf.getField().getOpPosition().getY();
		switch (move) {
		case DOWN:
			// ???? ???? ? ?? ?? ?????????? ?????(??? ? ??
			// ?? blocks ????+ ? ???? ?????? ???? ? ??)
			if (line + loops < height && grid[line + loops][column].equals(".")) {// ????? ?? ?? ?? ????
																					// ???? ?? ????? , ?
																					// ??
				// ?? ?? ??????? ?? ????? ? ???? ?? ??
				score++;
				if (line > ((height - 1) / 2)) {// if x=[8,14]
					// ????? ?? ?????? ? ??????"??????? - ?????? ??? ,
					// ??????? ?????, ?????
					// ???? ? ??? ??? ????????? ?????????? ?????-
					// ????? ? bot ? ?? ?? ? ?? ?? ??
					// ??????? ? ?? ?? ??????????? ? ???????????
					// ?????? ??? ? ???? ??? ? ? ??? ?? ?????
					// ??????? ? ?????? ? ?? ??? ? ?? ??
					// ????????? ?????????????? ??? ??? ????????
					// ????? ? 8????? ??? ? 14?????.?? ????????
					// ? ???.
					score++;
				} else { // ? ????????? ???????? ???????? ??- ??? ???
					// ??????. ???;! ????? ? ???? ??? ???
					// ?? ?? ??? ?? ????? ?????? ??? => ???? ?? ????? ?
					// ??? ?????? ?????, ??? ??? ??? ???? ????
					// ???????????? ???? ???? ????? ??? ??-????
					// ????? ?? ??? , ? "????? ? ???? - ????
					// ? ??? ?? ?? ???
					switch (previousMove) {// zig zag moves buy some time
					// ??? ???? ? ????;! ? ???;! ???????(??? ? ?? ???
					// ???? , ??? ??? ??- ??? ????? ??? )???? ??
					// ????????
					case LEFT:
						score++;
						break;
					case RIGHT:
						score++;
						break;
					}
				}
			}else {
				score--;
			}

		case UP:
			// ? ???? ??? ??????? ?? ? ? ?? ???
			if ((line - loops > 0) && (grid[line - loops][column].equals("."))) {
				score++;
				if (line <= ((height - 1) / 2)) {// if x=[1,7]
					score++;
				} else {
					switch (previousMove) {// zig zag moves buy some time
					case LEFT:
						score++;
						break;
					case RIGHT:
						score++;
						break;
					}
				}
			}else {
				score--;
			}
			break;
		case LEFT:
			if ((column - loops > 0) && (grid[line][column - loops].equals("."))) {
				score++;
				if (column > ((width - 1) / 2)) {// if y=[8,14]
					score++;
				} else {
					switch (previousMove) {// zig zag moves buy some time
					case UP:
						score++;
						break;
					case DOWN:
						score++;
						break;
					}
				}
				//apply on more tactic - tactic3 => steel space from your opponent
				if(grid[line][column - 1].equals(".")) {
					score=score+2;//gain space will increase possibilities of winning too
				}
			}else {
				score--;
			}
			break;
		case RIGHT:
			if ((column + loops < width) && (grid[line][column + loops].equals("."))) {
				score++;
				if (column > ((width - 1) / 2)) {// if y=[1,7]
					score++;
				} else {
					switch (previousMove) {// zig zag moves buy some time
					case UP:
						score++;
						break;
					case DOWN:
						score++;
						break;
					}
				}
				//apply on more tactic - tactic3 => steel space from your opponent
				if(grid[line][column + 1].equals(".")) {
					score=score+2;//gain space will increase possibilities of winning too
				}
			}else {
				score--;
			}
			break;
		}

		return score + findScroceApplyTactics(leaf, move, loops - 1);

	}
	/**
	 * Another heuristic algorithm. As empty block but with zig-zag
	 * 
	 * @param leaf
	 * @param move
	 * @param loops
	 *            -How long algorithm has to search.
	 * @return
	 */
	public static int findScroceZigZagGreedy(Node leaf, MoveType move, int loops) {
		if (loops == 0)
			return 0;
		MoveType previousMove = leaf.getPreMove();
		int score = 0;
		String[][] grid = leaf.getField().getField();
		int column = (int) leaf.getField().getOpPosition().getX();// Leaf is a min node. So we have to find the score of
																	// opponent.
		int line = (int) leaf.getField().getOpPosition().getY();
		switch (move) {

		case DOWN:
			if (line + loops < height && grid[line + loops][column].equals("."))
				score++;
			switch (previousMove) {// zig zag moves buy some time
			case RIGHT:
				score++;
				break;
			case LEFT:
				score++;
				break;
			}
			//steel space
			if(line< (( height  - 1) / 2) + 2){
				score++;
			}
			break;

		case UP:
			if ((line - loops > 0) && (grid[line - loops][column].equals(".")))
				score++;
			switch (previousMove) {// zig zag moves buy some time
			case RIGHT:
				score++;
				break;
			case LEFT:
				score++;
				break;
			}
			//steal space if you near middle
			if(line> (( height  - 1) / 2) + 2){
				score++;
			}
			break;

		case LEFT:
			if ((column - loops > 0) && (grid[line][column - loops].equals(".")))
				score++;
			switch (previousMove) {// zig zag moves buy some time
			case UP:
				score++;
				break;
			case DOWN:
				score++;
				break;
			}
			//steal space if you near middle
			if(column> ((width - 1) / 2) + 2){
				score++;
			}
			break;

		case RIGHT:
			if ((column + loops < width) && (grid[line][column + loops].equals(".")))
				score++;
			switch (previousMove) {// zig zag moves buy some time
			case UP:
				score++;
				break;
			case DOWN:
				score++;
				break;
			}
			//steal space if you near middle
			if(column< ((width - 1) / 2) + 2){
				score++;
			}
			break;
		}
		return score + findScroceZigZagGreedy(leaf, move, loops - 1);

	}
	
	/**
	 * A heuristic algorithm. Score is bigger when available moves have more empty
	 * neighboring cells.
	 * 
	 * @param leaf
	 *            -The current state.
	 * @param move
	 *            -The next move.
	 * @param loops
	 *            -How long algorithm has to search.
	 * @return int -Score
	 */
	private static int findScore(Node leaf, MoveType move, int loops) {
		if (loops == 0)
			return 0;
		int score = 0;
		String[][] grid = leaf.getField().getField();
		int column = (int) leaf.getField().getOpPosition().getX();// Leaf is a min node. So we have to find the score of
																	// opponent.
		int line = (int) leaf.getField().getOpPosition().getY();
		switch (move) {
		case DOWN:

			if ((column + 1 < width) && (line + loops < height))
				if (grid[line + loops][column + 1].equals("."))
					score++;

			if ((column - 1 > 0) && (line + loops < height))
				if (grid[line + loops][column - 1].equals("."))
					score++;

			break;
		case UP:
			if ((column + 1 < width) && (line - loops > 0))
				if (grid[line - loops][column + 1].equals("."))
					score++;

			if ((column - 1 > 0) && (line - loops > 0))
				if (grid[line - loops][column - 1].equals("."))
					score++;

			break;
		case LEFT:
			if ((column - loops > 0) && (line - 1 > 0))
				if (grid[line - 1][column - loops].equals("."))
					score++;
			if ((column - loops > 0) && (line + 1 < height))
				if (grid[line + 1][column - loops].equals("."))
					score++;
			break;
		case RIGHT:
			if ((column + loops < width) && (line - 1 > 0))
				if (grid[line - 1][column + loops].equals("."))
					score++;
			if ((column + loops < width) && (line + 1 < height))
				if (grid[line + 1][column + loops].equals("."))
					score++;
			break;
		}

		return score + findScore(leaf, move, loops - 1);
	}

	/**
	 * Returns a node, that could be the next state of parent. If parent is MAX, new
	 * node will be MIN, and vice versa. So child.myid = parent.Opid.
	 * 
	 * @param move
	 *            -The next move.
	 * @param parent
	 *            -The current state.
	 * @return Node - A new Node.
	 */
	private static Node makeNextPossibleMove(MoveType move, Node parent) {
		String type;

		if (parent.getType().equals("MAX"))
			type = "MIN";
		else
			type = "MAX";

		int column = (int) parent.getField().getMyPosition().getX();
		int line = (int) parent.getField().getMyPosition().getY();


		String[][] grid = new String[height][width];

		for (int x = 0; x < height; x++)
			for (int y = 0; y < width; y++)
				grid[x][y] = parent.getField().getField()[x][y] ;

		String mark = grid[line][column];
		grid[line][column] = "x";

		switch (move) {
		case UP:
			line--;
			break;
		case DOWN:
			line++;
			break;
		case RIGHT:
			column++;
			break;
		case LEFT:
			column--;
			break;
		}
		grid[line][column] = mark;

		// make new Field for the opponent
		Field NewField = new Field();
		NewField.setMyId(parent.getField().getOpId());// Set myId and opponent's id
		NewField.setField(grid);
		NewField.setMyPosition(parent.getField().getOpPosition());
		NewField.SetOpPosition(new Point(column, line));
		return new Node(parent, NewField, move, type);
	}

	/**
	 * Check the moves that the player should not to make.
	 * 
	 * @param field
	 *            -current state
	 * @param state
	 *            - state of bot
	 * @return Array of MoveType
	 */
	private static ArrayList<MoveType> findForbiddens(Field field) {

		ArrayList<MoveType> forbiddens = new ArrayList<>();

		int column = (int) field.getMyPosition().getX();
		int line = (int) field.getMyPosition().getY();
		String[][] grid = field.getField();

		if (line == 0) {
			addToForb(MoveType.UP, forbiddens); // Check for limit

		} else if (!grid[line - 1][column].equals(".")) {
			addToForb(MoveType.UP, forbiddens);// Check if there is empty
		}
		if (column == 0) {
			addToForb(MoveType.LEFT, forbiddens);

		} else if (!grid[line][column - 1].equals(".")) {
			addToForb(MoveType.LEFT, forbiddens);
		}

		if (column == width - 1) {
			addToForb(MoveType.RIGHT, forbiddens);

		} else if (!grid[line][column + 1].equals(".")) {
			addToForb(MoveType.RIGHT, forbiddens);
		}

		if (line == height - 1) {
			addToForb(MoveType.DOWN, forbiddens);
		} else if (!grid[line + 1][column].equals(".")) {
			addToForb(MoveType.DOWN, forbiddens);
		}
		return forbiddens;
	}

	/**
	 * If List doesn't contain the move add it, otherwise do nothing.
	 * 
	 * @param move
	 * @param forbiddens
	 * @return
	 */
	private static void addToForb(MoveType move, ArrayList<MoveType> forbiddens) {
		if (!forbiddens.contains(move)) forbiddens.add(move);
	}

	/**
	 * Another heuristic algorithm. Better move has more empty cells in front of it.
	 * 
	 * @param leaf
	 * @param move
	 * @param loops
	 *            -How long algorithm has to search.
	 * @return
	 */
	private static int findScoreByFrontEmptyBlocks(Node leaf, MoveType move, int loops) {
		if (loops == 0)
			return 0;
		int score = 0;
		String[][] grid = leaf.getField().getField();

		int column = (int) leaf.getField().getOpPosition().getX();// Leaf is a min node. So we have to find the score of
																	// opponent.
		int line = (int) leaf.getField().getOpPosition().getY();
		switch (move) {

		case DOWN:
			if (line + loops < height && grid[line + loops][column].equals("."))
				score++;
			break;

		case UP:
			if ((line - loops > 0) && (grid[line - loops][column].equals(".")))
				score++;
			break;

		case LEFT:
			if ((column - loops > 0) && (grid[line][column - loops].equals(".")))
				score++;
			break;

		case RIGHT:
			if ((column + loops < width) && (grid[line][column + loops].equals(".")))
				score++;
			break;
		}
		if(line >= height -1 || line <= 0) score--;
		if(column >= width - 1 || column <=0) score--;
		return score + findScoreByFrontEmptyBlocks(leaf, move, loops - 1);
	}


}
	
