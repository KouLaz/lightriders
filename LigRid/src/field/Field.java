// // Copyright 2016 riddles.io (developers@riddles.io)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//        http://www.apache.org/licenses/LICENSE-2.0
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//  
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
package field;

import java.awt.Point;

/**
 * field.Field
 *
 * Handles everything that has to do with the field, such as storing the current
 * state and performing calculations on the field.
 *
 * @author Jim van Eeden <jim@riddles.io>
 */
public class Field {

    private int myId;
    private int opId;
    private int width;
    private int height;
    private String[][] field;
    // Point(column,line)==>(x,y)
    private Point myPosition;
    private Point opPosition;

    /**
     * Initializes and clears field
     *
     * @throws Exception exception
     */
    public void initField() throws Exception {
        try {
            this.field = new String[this.width][this.height];
        } catch (Exception e) {
            throw new Exception("Error: trying to initialize field while field "
                    + "settings have not been parsed yet.");
        }

        clearField();
    }

    /**
     * Clears the field
     */
    private void clearField() {
        for (int line = 0; line < this.height; line++) {
            for (int column = 0; column < this.width; column++) {
                this.field[line][column] = ".";
            }
        }

        this.myPosition = null;
    }

    /**
     * Parse field from comma separated String
     *
     * @param s input from engine
     */
    public void parseFromString(String s) {
        clearField();

        String[] split = s.split(",");
        int column = 0;
        int line = 0;

        for (String value : split) {
            this.field[line][column] = value;

            if (this.field[line][column].equals(this.myId + "")) {
                this.myPosition = new Point(column, line);
            }else if(this.field[line][column].equals(this.opId + "")) {
            	this.opPosition = new Point(column,line);
            }

            if (++column == this.width) {
                column = 0;
                line++;
            }
        }
    }

    public void setMyId(int id) {
        this.myId = id;
        if(id==0) this.opId=1;
        else this.opId=0;
    }
    
    public int getMyId() {
    	return this.myId;
    }
    public int getOpId() {
    	return this.opId;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getWidth(){
        return this.width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight(){
        return this.height;
    }

    public String[][] getField() {
        return this.field;
    }
    public void setField(String[][] field) {
    	this.field = field;
    }

    public Point getMyPosition() {
        return this.myPosition;
    }
    
    public void setMyPosition(Point pos) {
    	this.myPosition=pos;
    }
    
    public Point getOpPosition() {
        return this.opPosition;
    }
    
    public void SetOpPosition(Point pos) {
    	this.opPosition=pos;
    }
}
